# GP2 Tests

## Prereqs

Any modern Linux or Mac running Docker CE 19.03 or greater will suffice.

## Running

The test suite is split up into "quick", "prog", and "slow" tests.

```
$ ./test.sh [-q] [-v] quick latest
```

```
$ ./test.sh [-q] [-v] prog latest
```

```
$ ./test.sh [-q] [-v] slow latest
```

`-q` is quiet mode, and `-v` is verbose.

Replace `latest` with the branch you'd like to run the tests against.
