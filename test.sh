#!/bin/bash

if [[ -n "$4" ]]; then
  echo "UNKNOWN COMMAND."
  exit 1
fi

if [[ -n "$3" ]]; then
  TEST_SUITE=$2
  GP2_TAG=$3
  if [ $1 == "-v" ]; then
    TEST_MODE=2
  elif [ $1 == "-q" ]; then
    TEST_MODE=0
  else
    echo "UNKNOWN COMMAND."
    exit 1
  fi
else
  TEST_SUITE=$1
  GP2_TAG=$2
  TEST_MODE=1
fi

if [ ! -f "suites/$TEST_SUITE.sh" ]; then
  echo "UNKNOWN COMMAND."
  exit 1
fi

if [ $TEST_MODE -gt 0 ]; then
  echo ""
  if [[ "$TEST_SUITE" == "quick" ]]; then
    echo "     ██████╗ ██████╗ ██████╗      ██████╗ ██╗   ██╗██╗ ██████╗██╗  ██╗     "
    echo "    ██╔════╝ ██╔══██╗╚════██╗    ██╔═══██╗██║   ██║██║██╔════╝██║ ██╔╝     "
    echo "    ██║  ███╗██████╔╝ █████╔╝    ██║   ██║██║   ██║██║██║     █████╔╝      "
    echo "    ██║   ██║██╔═══╝ ██╔═══╝     ██║▄▄ ██║██║   ██║██║██║     ██╔═██╗      "
    echo "    ╚██████╔╝██║     ███████╗    ╚██████╔╝╚██████╔╝██║╚██████╗██║  ██╗     "
    echo "     ╚═════╝ ╚═╝     ╚══════╝     ╚══▀▀═╝  ╚═════╝ ╚═╝ ╚═════╝╚═╝  ╚═╝     "
  elif [[ "$TEST_SUITE" == "prog" ]]; then
    echo "      ██████╗ ██████╗ ██████╗      ██████╗ ██████╗  ██████╗  ██████╗       "
    echo "     ██╔════╝ ██╔══██╗╚════██╗     ██╔══██╗██╔══██╗██╔═══██╗██╔════╝       "
    echo "     ██║  ███╗██████╔╝ █████╔╝     ██████╔╝██████╔╝██║   ██║██║  ███╗      "
    echo "     ██║   ██║██╔═══╝ ██╔═══╝      ██╔═══╝ ██╔══██╗██║   ██║██║   ██║      "
    echo "     ╚██████╔╝██║     ███████╗     ██║     ██║  ██║╚██████╔╝╚██████╔╝      "
    echo "      ╚═════╝ ╚═╝     ╚══════╝     ╚═╝     ╚═╝  ╚═╝ ╚═════╝  ╚═════╝       "
  else
    echo "      ██████╗ ██████╗ ██████╗     ███████╗██╗      ██████╗ ██╗    ██╗      "
    echo "     ██╔════╝ ██╔══██╗╚════██╗    ██╔════╝██║     ██╔═══██╗██║    ██║      "
    echo "     ██║  ███╗██████╔╝ █████╔╝    ███████╗██║     ██║   ██║██║ █╗ ██║      "
    echo "     ██║   ██║██╔═══╝ ██╔═══╝     ╚════██║██║     ██║   ██║██║███╗██║      "
    echo "     ╚██████╔╝██║     ███████╗    ███████║███████╗╚██████╔╝╚███╔███╔╝      "
    echo "      ╚═════╝ ╚═╝     ╚══════╝    ╚══════╝╚══════╝ ╚═════╝  ╚══╝╚══╝       "
  fi
  echo ""

  echo "████████╗███████╗███████╗████████╗    ███████╗██╗   ██╗██╗████████╗███████╗"
  echo "╚══██╔══╝██╔════╝██╔════╝╚══██╔══╝    ██╔════╝██║   ██║██║╚══██╔══╝██╔════╝"
  echo "   ██║   █████╗  ███████╗   ██║       ███████╗██║   ██║██║   ██║   █████╗  "
  echo "   ██║   ██╔══╝  ╚════██║   ██║       ╚════██║██║   ██║██║   ██║   ██╔══╝  "
  echo "   ██║   ███████╗███████║   ██║       ███████║╚██████╔╝██║   ██║   ███████╗"
  echo "   ╚═╝   ╚══════╝╚══════╝   ╚═╝       ╚══════╝ ╚═════╝ ╚═╝   ╚═╝   ╚══════╝"

  echo ""
  echo "======== PULLING DOCKER IMAGES ========"
  echo ""
fi

if [ $TEST_MODE -eq 0 ]; then
  docker pull -q registry.gitlab.com/yorkcs/batman/gp2i:legacy > /dev/null
  if [[ "$GP2_TAG" != "local" ]]; then
    if [[ "$GP2_TAG" != "legacy" ]]; then
      docker pull -q registry.gitlab.com/yorkcs/batman/gp2i:$GP2_TAG > /dev/null
    fi
  fi
elif [ $TEST_MODE -eq 1 ]; then
  docker pull -q registry.gitlab.com/yorkcs/batman/gp2i:legacy
  if [[ "$GP2_TAG" != "local" ]]; then
    if [[ "$GP2_TAG" != "legacy" ]]; then
      docker pull -q registry.gitlab.com/yorkcs/batman/gp2i:$GP2_TAG
    fi
  fi
else
  docker pull registry.gitlab.com/yorkcs/batman/gp2i:legacy
  if [[ "$GP2_TAG" != "local" ]]; then
    if [[ "$GP2_TAG" != "legacy" ]]; then
      docker pull registry.gitlab.com/yorkcs/batman/gp2i:$GP2_TAG
    fi
  fi
fi

if [ $TEST_MODE -gt 0 ]; then
  echo ""
  echo "========= TEST SUITE STARTING ========="
  echo ""
fi

GREEN='\033[1;32m'
BLUE='\033[0;34m'
GREY='\033[1;30m'
RED='\033[1;31m'
NC='\033[0m'

PASSED=0
FAILED=0
CURRENT=0

gp_cat () {
  echo -e -n "${BLUE}"
  RAW_CONTENT=$(sed '/^$/d' $1)
  if [ $TEST_MODE -eq 2 ]; then
    echo -e "${RAW_CONTENT}${NC}"
  else
    TRIMMED_CONTENT=$(echo "$RAW_CONTENT" | head -c1000)
    if [[ "$RAW_CONTENT" == "$TRIMMED_CONTENT" ]]; then
      echo -e "${RAW_CONTENT}${NC}"
    else
      echo -e "${TRIMMED_CONTENT}${NC}"
      echo "..."
    fi
  fi
}

gp_ok () {
  ((PASSED++))
  if [ $TEST_MODE -eq 0 ]; then
    echo -e -n "."
  else
    echo -e "${GREEN}OK!${NC}"
  fi
}

gp_fail () {
  ((FAILED++))
  if [ $TEST_MODE -eq 0 ]; then
    echo -e -n "${RED}F${NC}"
  else
    echo -e "${RED}FAIL!${NC}"
  fi
}

gp_cleanup() {
  rm gp2.log gp2run.log &> /dev/null || true
  rm -rf tmp &> /dev/null || true
}

gp_test () {
  ((CURRENT++))
  if [ $TEST_MODE -gt 0 ]; then
    echo -e -n "${GREY}${CURRENT}.${NC} Testing ${GREY}$1${NC} program with ${GREY}$2${NC}... "
  fi
  gp_cleanup
  mkdir tmp
  if [ -z "${GP2_FLAGS}" ]; then
    docker run -v ${PWD}:/data -e MAX_NODES='1000000' -e MAX_EDGES='1000000' \
    --rm registry.gitlab.com/yorkcs/batman/gp2i:$GP2_TAG \
    programs/$1 graphs/$2 &> tmp/gp2.out || true
  else
    docker run -v ${PWD}:/data -e MAX_NODES='1000000' -e MAX_EDGES='1000000' \
    -e GP2_FLAGS="${GP2_FLAGS}" \
    --rm registry.gitlab.com/yorkcs/batman/gp2i:$GP2_TAG \
    programs/$1 graphs/$2 &> tmp/gp2.out || true
  fi

  if [ -f "gp2run.log" ]; then
    cp gp2run.log debug/$CURRENT.log
    if [ -f "tmp/gp2.out" ]; then
      cp tmp/gp2.out debug/$CURRENT.out
    fi
  elif [ -f "gp2.log" ]; then
    cp gp2.log debug/$CURRENT.log
    if [ -f "tmp/gp2.out" ]; then
      cp tmp/gp2.out debug/$CURRENT.out
    fi
  elif [ -f "tmp/gp2.out" ]; then
    cp tmp/gp2.out debug/$CURRENT.log
  fi

  if [ "$(head -c 1 outputs/$3)" == "[" ] ; then
    if [ "$(head -c 1 tmp/gp2.out)" == "[" ] ; then
      if is_iso $3 || ([[ -n "$4" ]] && is_iso $4) || ([[ -n "$5" ]] && is_iso $5) ; then
        gp_ok
      else
        gp_fail
        if [ $TEST_MODE -gt 0 ]; then
          echo "EXPECTED UP TO ISOMORPHISM:"
          gp_cat outputs/$3
          if [[ -n "$4" ]]; then
            echo "OR:"
            gp_cat outputs/$4
          fi
          if [[ -n "$5" ]]; then
            echo "OR:"
            gp_cat outputs/$5
          fi
          echo "BUT ACTUALLY SAW:"
          gp_cat tmp/gp2.out
        fi
      fi
    else
      gp_fail
      if [ $TEST_MODE -gt 0 ]; then
        echo "EXPECTED UP TO ISOMORPHISM:"
        gp_cat outputs/$3
        echo "BUT ACTUALLY SAW:"
        gp_cat tmp/gp2.out
      fi
    fi
  else
    if cmp -s "tmp/gp2.out" "outputs/$3"; then
      gp_ok
    else
      gp_fail
      if [ $TEST_MODE -gt 0 ]; then
        echo "EXPECTED EXACTLY:"
        gp_cat outputs/$3
        echo "BUT ACTUALLY SAW:"
        gp_cat tmp/gp2.out
      fi
    fi
  fi
}

is_iso() {
  export PROG="$(cat outputs/$1)"
  envsubst < iso.gp2 > tmp/iso.gp2
  docker run -v ${PWD}:/data -e MAX_NODES='1000000' -e MAX_EDGES='1000000' \
    -e GP2_FLAGS="-m" --rm registry.gitlab.com/yorkcs/batman/gp2i:legacy \
    tmp/iso.gp2 tmp/gp2.out &> tmp/iso.out || true
  [[ "$(head -c 5 tmp/iso.out)" == "[ | ]" ]]
  return
}

rm -rf debug &> /dev/null || true
mkdir debug

START=`date +%s`

. suites/$TEST_SUITE.sh

gp_cleanup

if [ $TEST_MODE -eq 0 ]; then
  echo ""
  echo "$PASSED/$CURRENT in" $(expr `date +%s` - $START)s
else
  echo ""
  echo "========= TEST SUITE FINISHED ========="
  echo ""
  echo "PASSED: $PASSED"
  echo "FAILED: $FAILED"
  echo "RUNTIME:" $(expr `date +%s` - $START)s
  echo ""
  echo "========= END OF TEST SUMMARY ========="
  echo ""
fi

[ $FAILED -eq 0 ]
